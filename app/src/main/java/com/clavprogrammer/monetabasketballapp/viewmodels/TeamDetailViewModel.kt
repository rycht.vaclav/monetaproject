package com.clavprogrammer.monetabasketballapp.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.clavprogrammer.app_domain.features.team.usecases.GetSingleTeamUseCase
import com.clavprogrammer.app_domain.models.Team
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

/*
* Created by Václav Rychtecký on 01/14/2024
*/
class TeamDetailViewModel(
    private val getSingleTeamUseCase: GetSingleTeamUseCase,
    private val teamId: Int
) : ViewModel() {

    private val _detail = MutableStateFlow<Team?>(null)
    val detail = _detail

    init {
        viewModelScope.launch(Dispatchers.IO) {
            val result = getSingleTeamUseCase(teamId)
            if (result.isSuccess()) {
                val team = result.getOrNull()
                if (team != null) {
                    _detail.emit(team)
                }
            }
        }
    }
}