package com.clavprogrammer.monetabasketballapp.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.clavprogrammer.app_domain.features.player.usecases.GetSinglePlayerUseCase
import com.clavprogrammer.app_domain.models.Player
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

/*
* Created by Václav Rychtecký on 01/13/2024
*/
class PlayerDetailViewModel(
    private val getSinglePlayerUseCase: GetSinglePlayerUseCase,
    private val playerId: Int
) :
    ViewModel() {

    private val _detail = MutableStateFlow<Player?>(null)
    val detail = _detail

    init {
        viewModelScope.launch(Dispatchers.IO) {
            val result = getSinglePlayerUseCase(playerId)
            if (result.isSuccess()) {
                val player = result.getOrNull()
                if (player != null) {
                    _detail.emit(player)
                }
            }
        }
    }
}