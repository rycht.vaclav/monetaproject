package com.clavprogrammer.monetabasketballapp.viewmodels

import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.clavprogrammer.app_domain.features.player.usecases.FetchPlayersUseCase
import com.clavprogrammer.app_domain.models.Player
import com.clavprogrammer.monetabasketballapp.pagging.PlayerPagingSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.launch
import timber.log.Timber

/*
* Created by Václav Rychtecký on 01/12/2024
*/
class PlayersListViewModel(
    private val fetchPlayersUseCase: FetchPlayersUseCase,
) : ViewModel() {

    var playersList: Flow<PagingData<Player>> = emptyFlow()
    private val pager = Pager(config = PagingConfig(35)) {
        PlayerPagingSource(fetchPlayersUseCase)
    }.flow

    init {
        playersList = pager.cachedIn(viewModelScope)
    }
}