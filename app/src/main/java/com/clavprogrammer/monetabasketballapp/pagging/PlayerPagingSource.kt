package com.clavprogrammer.monetabasketballapp.pagging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.clavprogrammer.app_domain.features.player.usecases.FetchPlayersUseCase
import com.clavprogrammer.app_domain.models.Player
import com.clavprogrammer.app_domain.models.Players
import java.lang.IllegalStateException

/*
* Created by Václav Rychtecký on 01/13/2024
*/
class PlayerPagingSource(private val fetchPlayersUseCase: FetchPlayersUseCase) :
    PagingSource<Int, Player>() {


    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Player> {

        val nextPageNumber = params.key ?: 1
        val result = fetchPlayersUseCase(nextPageNumber)
        val resultData = result.getOrNull()

        return if (result.isSuccess() && resultData != null) {
            LoadResult.Page(
                data = resultData.data,
                prevKey = if (nextPageNumber == 1) null else nextPageNumber - 1,
                nextKey = if (nextPageNumber == resultData.meta.totalPages) null else nextPageNumber + 1
            )
        } else {
            LoadResult.Error(
                throwable = IllegalStateException("Pager failed load data")
            )
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Player>): Int? {
        return null
    }

}