package com.clavprogrammer.monetabasketballapp

/*
* Created by Václav Rychtecký on 01/12/2024
*/
object Constants {
    //Timber
    const val TIMBER_TAG = "MonetaBasketballApp"

    //Positions
    const val FORWARD = "Forward"
    const val CENTER = "Center"
    const val GUARD = "Guard"

    //Players
    const val PLAYER_NAME = "Player Name"
    const val POSITION = "Position"
    const val TEAM = "Team"
    const val HEIGHT = "Height"
    const val WEIGHT = "Weight"
    const val PLAYER_DETAIL = "Player detail"

    //Teams
    const val FULL_NAME = "Full Name"
    const val ABBREVIATION = "Abbreviation"
    const val CITY = "City"
    const val CONFERENCE = "Conference"
    const val DIVISION = "Division"
    const val TEAM_DETAIL = "Team detail"
}