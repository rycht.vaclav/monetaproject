package com.clavprogrammer.monetabasketballapp

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.lazy.grid.LazyGridItemScope
import androidx.compose.foundation.lazy.grid.LazyGridScope
import androidx.compose.runtime.Composable
import androidx.paging.compose.LazyPagingItems
import timber.log.Timber

/*
* Created by Václav Rychtecký on 01/12/2024
*/
object Extensions {

    fun String.getPosition(): String {
        this.replace("-", "")
        var position = ""
        this.forEach {
            when (it) {
                'C' -> {
                    position += if (position.isEmpty()) Constants.CENTER else " - ${Constants.CENTER}"
                }

                'G' -> {
                    position += if (position.isEmpty()) Constants.GUARD else " - ${Constants.GUARD}"
                }

                'F' -> {
                    position += if (position.isEmpty()) Constants.FORWARD else " - ${Constants.FORWARD}"
                }

                else -> {
                    Timber.d("Position missing")
                }
            }
        }
        return position
    }

    fun Double?.weightToString(): String {
        return this?.toString() ?: "N/A"
    }

    @ExperimentalFoundationApi
    fun <T : Any> LazyGridScope.items(
        lazyPagingItems: LazyPagingItems<T>,
        itemContent: @Composable LazyGridItemScope.(value: T?) -> Unit
    ) {
        items(lazyPagingItems.itemCount) { index ->
            itemContent(lazyPagingItems[index])
        }
    }
}