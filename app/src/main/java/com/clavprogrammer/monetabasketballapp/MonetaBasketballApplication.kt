package com.clavprogrammer.monetabasketballapp

import android.app.Application
import com.clavprogrammer.app_data.di.appModule
import com.clavprogrammer.app_data.di.networkModule
import com.clavprogrammer.monetabasketballapp.di.viewModelModule
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.core.component.KoinComponent
import org.koin.core.context.startKoin
import timber.log.Timber

/*
* Created by Václav Rychtecký on 01/12/2024
*/
class MonetaBasketballApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(CustomTagTree(Constants.TIMBER_TAG))
        }

        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@MonetaBasketballApplication)
            modules(
                appModule + viewModelModule + networkModule
            )
        }
    }

    inline fun <reified T : Any> getKoinInstance(): T {
        return object : KoinComponent {
            val value: T by inject()
        }.value
    }
}

class CustomTagTree(private val customTag: String) : Timber.DebugTree() {
    override fun createStackElementTag(element: StackTraceElement): String {
        return customTag
    }
}