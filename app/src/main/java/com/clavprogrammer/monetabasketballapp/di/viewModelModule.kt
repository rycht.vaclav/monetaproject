package com.clavprogrammer.monetabasketballapp.di

import android.content.Context
import android.net.ConnectivityManager
import com.clavprogrammer.monetabasketballapp.viewmodels.PlayerDetailViewModel
import com.clavprogrammer.monetabasketballapp.viewmodels.PlayersListViewModel
import com.clavprogrammer.monetabasketballapp.viewmodels.TeamDetailViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

/*
* Created by Václav Rychtecký on 01/12/2024
*/
val viewModelModule: Module = module {
    viewModel { PlayersListViewModel(get()) }
    viewModel { (playerId: Int) -> PlayerDetailViewModel(get(), playerId = playerId) }
    viewModel { (teamId: Int) -> TeamDetailViewModel(get(), teamId = teamId) }
}