package com.clavprogrammer.monetabasketballapp.ui.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.clavprogrammer.app_domain.models.Player
import com.clavprogrammer.monetabasketballapp.Constants
import com.clavprogrammer.monetabasketballapp.Extensions.getPosition
import com.clavprogrammer.monetabasketballapp.Extensions.weightToString

/*
* Created by Václav Rychtecký on 01/15/2024
*/
@Composable
fun PlayerDetailView(player: Player, onClick: (Int) -> Unit) {
    Column(
        modifier = Modifier
            .verticalScroll(rememberScrollState())
    ) {
        DetailItem(
            modifier = Modifier.padding(top = 16.dp),
            title = Constants.PLAYER_NAME,
            text = "${player.firstName} ${player.lastName}",
            isFirst = true
        )
        DetailItem(
            title = Constants.POSITION,
            text = player.position.getPosition().ifBlank { "N/A" })
        DetailItem(
            title = Constants.TEAM,
            text = player.team.fullName,
            isClickable = true,
            onClick = {
                onClick(player.team.id)
            })
        DetailItem(
            title = Constants.HEIGHT,
            text = formatHeight(player.heightFeet, player.heightInches)
        )
        DetailItem(
            title = Constants.WEIGHT,
            text = player.weightPounds.weightToString(),
            isLast = true
        )
    }
}

fun formatHeight(feet: Int?, inches: Int?): String {
    return if (feet == null && inches == null) {
        "N/A"
    } else {
        "$feet'$inches''"
    }
}