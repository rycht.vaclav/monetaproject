package com.clavprogrammer.monetabasketballapp.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.material3.Divider
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.bumptech.glide.integration.compose.placeholder
import com.clavprogrammer.monetabasketballapp.Constants
import com.clavprogrammer.monetabasketballapp.ContentDescription
import com.clavprogrammer.monetabasketballapp.R
import com.clavprogrammer.monetabasketballapp.ui.components.DetailItem
import com.clavprogrammer.monetabasketballapp.ui.components.HugeText
import com.clavprogrammer.monetabasketballapp.ui.components.PlayerDetailView
import com.clavprogrammer.monetabasketballapp.ui.components.TeamDetailView
import com.clavprogrammer.monetabasketballapp.ui.navigation.Destinations
import com.clavprogrammer.monetabasketballapp.viewmodels.TeamDetailViewModel
import timber.log.Timber

/*
* Created by Václav Rychtecký on 01/12/2024
*/
@Composable
fun TeamDetail(navController: NavHostController, viewModel: TeamDetailViewModel) {

    val teamDetail = viewModel.detail.collectAsState()

    Box(modifier = Modifier.fillMaxSize()) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            HugeText(
                modifier = Modifier
                    .height(48.dp)
                    .padding(top = 16.dp),
                text = Constants.TEAM_DETAIL
            )
            Divider(modifier = Modifier.padding(top = 16.dp), color = Color.Black, thickness = 1.dp)
            teamDetail.value?.let { team ->
                TeamDetailView(teamDetail = team)
            }
        }
        Image(
            modifier = Modifier
                .align(Alignment.TopStart)
                .size(48.dp)
                .padding(top = 16.dp, start = 16.dp)
                .clickable(
                    interactionSource = remember { MutableInteractionSource() },
                    indication = rememberRipple(
                        color = Color.Black
                    ),
                    onClick = {
                        navController.popBackStack()
                    }
                ),
            imageVector = Icons.Default.ArrowBack,
            contentDescription = ContentDescription.ARROW_BACK
        )
    }
}