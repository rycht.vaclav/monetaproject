package com.clavprogrammer.monetabasketballapp.ui.screens

import android.util.Log
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.paging.LoadState
import com.clavprogrammer.monetabasketballapp.Extensions.items
import com.clavprogrammer.monetabasketballapp.ui.components.PlayersListItem
import com.clavprogrammer.monetabasketballapp.ui.navigation.Destinations
import com.clavprogrammer.monetabasketballapp.viewmodels.PlayersListViewModel
import kotlinx.coroutines.delay

/*
* Created by Václav Rychtecký on 01/12/2024
*/
@OptIn(ExperimentalFoundationApi::class)
@Composable
fun PlayersList(navController: NavHostController, viewModel: PlayersListViewModel) {

    val players = viewModel.playersList.collectAsLazyPagingItems()

    if (players.itemCount == 0) {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            CircularProgressIndicator(modifier = Modifier.size(104.dp))
        }
    } else {
        Column {
            LazyVerticalGrid(
                modifier = Modifier.padding(top = 16.dp),
                columns = GridCells.Fixed(2),
                verticalArrangement = Arrangement.spacedBy(8.dp),
                horizontalArrangement = Arrangement.spacedBy(8.dp),
                contentPadding = PaddingValues(16.dp)
            ) {
                items(players) { player ->
                    player?.let {
                        PlayersListItem(player = it) { playerId ->
                            navController.navigate(
                                Destinations.PlayerDetailScreen.route + "/$playerId"
                            )
                        }
                    }
                }
            }
        }
    }
}