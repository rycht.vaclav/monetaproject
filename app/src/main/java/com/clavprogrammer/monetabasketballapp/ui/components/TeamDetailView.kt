package com.clavprogrammer.monetabasketballapp.ui.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.clavprogrammer.app_domain.models.Team
import com.clavprogrammer.monetabasketballapp.Constants

/*
* Created by Václav Rychtecký on 01/15/2024
*/
@Composable
fun TeamDetailView(teamDetail: Team) {
    Column(modifier = Modifier.verticalScroll(rememberScrollState())
    ) {
        DetailItem(
            modifier = Modifier.padding(top = 16.dp),
            title = Constants.FULL_NAME,
            text = teamDetail.fullName,
            isFirst = true
        )
        DetailItem(title = Constants.ABBREVIATION, text = teamDetail.abbreviation)
        DetailItem(title = Constants.CITY, text = teamDetail.city)
        DetailItem(title = Constants.CONFERENCE, text = teamDetail.conference)
        DetailItem(title = Constants.DIVISION, text = teamDetail.division, isLast = true)
    }
}