package com.clavprogrammer.monetabasketballapp.ui.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.clavprogrammer.app_domain.models.Player
import com.clavprogrammer.monetabasketballapp.Constants
import com.clavprogrammer.monetabasketballapp.Extensions.getPosition

/*
* Created by Václav Rychtecký on 01/12/2024
*/
@Composable
fun PlayersListItem(player: Player, func: (Int) -> Unit) {
    Card(
        modifier = Modifier.height(200.dp),
        border = BorderStroke(2.dp, Color.Black),
        colors = CardDefaults.cardColors(containerColor = Color.LightGray)
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(8.dp)
                .clickable {
                    func(player.id)
                },
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            LargeText(modifier = Modifier, text = Constants.PLAYER_NAME)
            Text(text = "${player.firstName} ${player.lastName}")
            LargeText(modifier = Modifier.padding(top = 4.dp), text = Constants.POSITION)
            Text(text = player.position.getPosition().ifBlank { "N/A" })
            LargeText(modifier = Modifier.padding(top = 4.dp), text = Constants.TEAM)
            Text(text = player.team.fullName, textAlign = TextAlign.Center)
        }
    }
}