package com.clavprogrammer.monetabasketballapp.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.material3.Divider
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

/*
* Created by Václav Rychtecký on 01/14/2024
*/
@Composable
fun DetailItem(
    modifier: Modifier = Modifier,
    title: String,
    text: String,
    isFirst: Boolean = false,
    isLast: Boolean = false,
    isClickable: Boolean = false,
    onClick: () -> Unit = {}
) {
    Box(modifier = Modifier.clickable(
        interactionSource = remember { MutableInteractionSource() },
        indication = if (isClickable) rememberRipple(color = Color.Black) else null,
        onClick = { onClick() }
    )) {
        Column {
            if(!isFirst) {
                Divider(modifier = modifier, color = Color.Black, thickness = 1.dp)
            }
            LargeText(modifier = Modifier.padding(top = 16.dp), text = title)
            NormalText(modifier = Modifier.padding(bottom = 16.dp), text = text)
            if (isLast) {
                Divider(color = Color.Black, thickness = 1.dp)
            }
        }
    }
}