package com.clavprogrammer.monetabasketballapp.ui.navigation

/*
* Created by Václav Rychtecký on 01/12/2024
*/
sealed class Destinations(val route: String) {
    object PlayersScreen : Destinations("players-screen")
    object PlayerDetailScreen : Destinations("player-detail-screen")
    object TeamDetailScreen : Destinations("clubs-screen")
}
