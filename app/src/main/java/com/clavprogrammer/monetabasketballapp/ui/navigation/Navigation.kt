package com.clavprogrammer.monetabasketballapp.ui.navigation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.lifecycle.viewmodel.compose.LocalViewModelStoreOwner
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.clavprogrammer.monetabasketballapp.ui.screens.PlayersDetail
import com.clavprogrammer.monetabasketballapp.ui.screens.PlayersList
import com.clavprogrammer.monetabasketballapp.ui.screens.TeamDetail
import com.clavprogrammer.monetabasketballapp.viewmodels.PlayerDetailViewModel
import com.clavprogrammer.monetabasketballapp.viewmodels.TeamDetailViewModel
import org.koin.androidx.compose.getViewModel
import org.koin.androidx.compose.koinViewModel
import org.koin.core.parameter.parametersOf
import timber.log.Timber

/*
* Created by Václav Rychtecký on 01/12/2024
*/
@Composable
fun Navigation() {
    val viewModelStoreOwner = checkNotNull(LocalViewModelStoreOwner.current) {
        "No ViewModelStoreOwner was provided via LocalViewModelStoreOwner"
    }

    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = Destinations.PlayersScreen.route) {
        composable(route = Destinations.PlayersScreen.route) {
            CompositionLocalProvider(
                LocalViewModelStoreOwner provides viewModelStoreOwner
            ) {
                Timber.d("Provide main screen")
                PlayersList(navController = navController, viewModel = koinViewModel())
            }
        }
        composable(route = Destinations.PlayerDetailScreen.route + "/{playerId}",
            arguments = listOf(
                navArgument("playerId") { type = NavType.IntType }
            )) { backStackEntry ->
            val playerId = backStackEntry.arguments?.getInt("playerId") ?: ""
            val playerDetailViewModel: PlayerDetailViewModel = getViewModel {
                parametersOf(playerId)
            }
            CompositionLocalProvider(
                LocalViewModelStoreOwner provides viewModelStoreOwner
            ) {
                PlayersDetail(
                    navController = navController,
                    viewModel = playerDetailViewModel
                )
            }
        }
        composable(route = Destinations.TeamDetailScreen.route + "/{teamId}",
            arguments = listOf(
                navArgument("teamId") { type = NavType.IntType }
            )) { backStackEntry ->
            val teamId = backStackEntry.arguments?.getInt("teamId") ?: ""
            val teamDetailViewModel: TeamDetailViewModel = getViewModel {
                parametersOf(teamId)
            }
            CompositionLocalProvider(
                LocalViewModelStoreOwner provides viewModelStoreOwner
            ) {
                TeamDetail(
                    navController = navController,
                    viewModel = teamDetailViewModel
                )
            }
        }
    }
}