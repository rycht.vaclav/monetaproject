package com.clavprogrammer.app_data.di

import com.clavprogrammer.app_data.features.players.PlayerRepositoryImpl
import com.clavprogrammer.app_data.features.players.PlayerSourceImpl
import com.clavprogrammer.app_data.features.teams.TeamRepositoryImpl
import com.clavprogrammer.app_data.features.teams.TeamSourceImpl
import com.clavprogrammer.app_domain.features.player.PlayerRepository
import com.clavprogrammer.app_domain.features.player.PlayerSource
import com.clavprogrammer.app_domain.features.player.usecases.FetchPlayersUseCase
import com.clavprogrammer.app_domain.features.player.usecases.GetSinglePlayerUseCase
import com.clavprogrammer.app_domain.features.team.TeamRepository
import com.clavprogrammer.app_domain.features.team.TeamSource
import com.clavprogrammer.app_domain.features.team.usecases.GetSingleTeamUseCase
import org.koin.dsl.module

/*
* Created by Václav Rychtecký on 01/12/2024
*/
val appModule = module {
    single { FetchPlayersUseCase(get()) }
    single { GetSinglePlayerUseCase(get()) }

    single<PlayerSource> { PlayerSourceImpl(get()) }
    single<PlayerRepository> { PlayerRepositoryImpl(get(), get()) }

    single { GetSingleTeamUseCase(get()) }

    single<TeamSource> { TeamSourceImpl() }
    single<TeamRepository> { TeamRepositoryImpl(get()) }
}