package com.clavprogrammer.app_data.di

import com.clavprogrammer.app_data.network.BallDontLieApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/*
* Created by Václav Rychtecký on 01/12/2024
*/
val networkModule = module {
    factory { provideOkHttpClient() }
    factory { provideBallDontLieApi(get()) }
    single { provideRetrofit(get()) }
}

fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .baseUrl("https://www.balldontlie.io/api/v1/")
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create()).build()
}

fun provideOkHttpClient(): OkHttpClient {
    val interceptor = HttpLoggingInterceptor()
    interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
    return OkHttpClient().newBuilder().addInterceptor(interceptor).build()
}

fun provideBallDontLieApi(retrofit: Retrofit): BallDontLieApi =
    retrofit.create(BallDontLieApi::class.java)