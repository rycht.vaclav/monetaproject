package com.clavprogrammer.app_data.network

import com.clavprogrammer.app_data.features.players.model.dto.PlayersDto
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

/*
* Created by Václav Rychtecký on 01/12/2024
*/
interface BallDontLieApi {
    @GET("players")
    fun fetchPlayers(
        @Query("per_page") perPage: Int,
        @Query("page") page: Int
    ): Call<PlayersDto>
}