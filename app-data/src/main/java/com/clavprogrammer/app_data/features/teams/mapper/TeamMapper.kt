package com.clavprogrammer.app_data.features.teams.mapper

import com.clavprogrammer.app_data.features.teams.model.dto.TeamDto
import com.clavprogrammer.app_domain.models.Team

/*
* Created by Václav Rychtecký on 01/14/2024
*/
object TeamMapper {

    fun mapToTeam(teamDto: TeamDto): Team {
        return Team(
            id = teamDto.id,
            abbreviation = teamDto.abbreviation,
            city = teamDto.city,
            conference = teamDto.conference,
            division = teamDto.division,
            fullName = teamDto.full_name,
            name = teamDto.name
        )
    }

    fun mapToTeam(teams: List<TeamDto>): List<Team> {
        return teams.map { mapToTeam(it) }
    }
}