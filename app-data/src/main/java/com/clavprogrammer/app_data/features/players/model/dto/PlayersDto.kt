package com.clavprogrammer.app_data.features.players.model.dto

/*
* Created by Václav Rychtecký on 01/12/2024
*/
data class PlayersDto(
    val data: List<PlayerDto>,
    val meta: MetaDto
)