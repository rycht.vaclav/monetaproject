package com.clavprogrammer.app_data.features.teams

import com.clavprogrammer.app_domain.ErrorResult
import com.clavprogrammer.app_domain.Result
import com.clavprogrammer.app_domain.features.team.TeamSource
import com.clavprogrammer.app_domain.models.Team

/*
* Created by Václav Rychtecký on 01/14/2024
*/
class TeamSourceImpl : TeamSource {
    lateinit var teams: List<Team>

    override fun getById(params: Int): Result<Team> {

        val team = teams.find { it.id == params }

        return if (team != null) {
            Result.Success(data = team)
        } else {
            Result.Error(ErrorResult("Empty result"))
        }
    }

    override fun getTeams(map: List<Team>) {
        teams = map
    }
}