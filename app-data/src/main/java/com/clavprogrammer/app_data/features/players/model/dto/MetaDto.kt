package com.clavprogrammer.app_data.features.players.model.dto

/*
* Created by Václav Rychtecký on 01/12/2024
*/
data class MetaDto(
    val total_pages: Int,
    val current_page: Int,
    val next_page: Int
)