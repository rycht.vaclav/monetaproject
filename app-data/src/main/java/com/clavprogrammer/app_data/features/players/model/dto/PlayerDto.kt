package com.clavprogrammer.app_data.features.players.model.dto

import com.clavprogrammer.app_data.features.teams.model.dto.TeamDto

/*
* Created by Václav Rychtecký on 01/12/2024
*/
data class PlayerDto (
    val id: Int,
    val first_name: String,
    val height_feet: Int?,
    val height_inches: Int?,
    val last_name: String,
    val position: String,
    val team: TeamDto,
    val weight_pounds: Double?
)