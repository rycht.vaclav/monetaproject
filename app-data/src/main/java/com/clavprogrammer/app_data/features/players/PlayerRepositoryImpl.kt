package com.clavprogrammer.app_data.features.players

import com.clavprogrammer.app_domain.ErrorResult
import com.clavprogrammer.app_domain.Result
import com.clavprogrammer.app_domain.features.player.PlayerRepository
import com.clavprogrammer.app_domain.features.player.PlayerSource
import com.clavprogrammer.app_domain.features.team.TeamSource
import com.clavprogrammer.app_domain.models.Meta
import com.clavprogrammer.app_domain.models.Player
import com.clavprogrammer.app_domain.models.Players

/*
* Created by Václav Rychtecký on 01/13/2024
*/
class PlayerRepositoryImpl(
    private val playerSource: PlayerSource,
    private val teamSource: TeamSource
) : PlayerRepository {

    var players: Players =
        Players(data = listOf(), meta = Meta(totalPages = 0, currentPage = 0, nextPage = 0))

    override suspend fun getPlayers(params: Int): Result<Players> {

        val result = playerSource.fetchPlayers(params)

        return if (result.isSuccess()) {
            val data = result.getOrNull()
            if (data != null) {
                players = Players(data = players.data + data.data, meta = data.meta)
                teamSource.getTeams(players.data.map { it.team })
                Result.Success(data = players)
            } else {
                Result.Error(error = ErrorResult(message = "Empty result"))
            }
        } else {
            Result.Error(error = ErrorResult(message = "Cant fetch data from server"))
        }
    }

    override suspend fun getSinglePlayer(params: Int): Result<Player> {

        val result = players.data.find { it.id == params }

        return if (result != null) {
            Result.Success(data = result)
        } else {
            Result.Error(error = ErrorResult(message = "Cannot find player by ID"))
        }
    }
}