package com.clavprogrammer.app_data.features.teams

import com.clavprogrammer.app_domain.Result
import com.clavprogrammer.app_domain.features.team.TeamRepository
import com.clavprogrammer.app_domain.features.team.TeamSource
import com.clavprogrammer.app_domain.models.Team

/*
* Created by Václav Rychtecký on 01/14/2024
*/
class TeamRepositoryImpl(
    private val teamSource: TeamSource,
) : TeamRepository {

    override suspend fun getTeamById(params: Int): Result<Team> {
        return teamSource.getById(params)
    }
}