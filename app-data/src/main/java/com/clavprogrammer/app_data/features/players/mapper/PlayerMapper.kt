package com.clavprogrammer.app_data.features.players.mapper

import com.clavprogrammer.app_data.features.players.model.dto.MetaDto
import com.clavprogrammer.app_data.features.players.model.dto.PlayerDto
import com.clavprogrammer.app_data.features.players.model.dto.PlayersDto
import com.clavprogrammer.app_data.features.teams.mapper.TeamMapper
import com.clavprogrammer.app_domain.models.Meta
import com.clavprogrammer.app_domain.models.Player
import com.clavprogrammer.app_domain.models.Players

/*
* Created by Václav Rychtecký on 01/13/2024
*/
object PlayerMapper {

    fun mapToPlayersFromDto(playersDto: PlayersDto): Players {
        return Players(
            data = mapToPlayers(playersDto.data),
            meta = mapToMeta(playersDto.meta)
        )
    }

    private fun mapToPlayers(playersDtoList: List<PlayerDto>): List<Player> {
        return playersDtoList.map {
            Player(
                id = it.id,
                firstName = it.first_name,
                lastName = it.last_name,
                position = it.position,
                heightFeet = it.height_feet,
                heightInches = it.height_inches,
                weightPounds = it.weight_pounds,
                team = TeamMapper.mapToTeam(it.team)
            )
        }
    }

    private fun mapToMeta(metaDto: MetaDto): Meta {
        return Meta(
            totalPages = metaDto.total_pages,
            currentPage = metaDto.current_page,
            nextPage = metaDto.next_page
        )
    }
}