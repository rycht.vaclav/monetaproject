package com.clavprogrammer.app_data.features.teams.model.dto

/*
* Created by Václav Rychtecký on 01/12/2024
*/
data class TeamDto(
    val id: Int,
    val abbreviation: String,
    val city: String,
    val conference: String,
    val division: String,
    val full_name: String,
    val name: String
)