package com.clavprogrammer.app_data.features.players

import com.clavprogrammer.app_data.features.players.mapper.PlayerMapper
import com.clavprogrammer.app_data.features.teams.mapper.TeamMapper
import com.clavprogrammer.app_data.network.BallDontLieApi
import com.clavprogrammer.app_domain.Result
import com.clavprogrammer.app_domain.exceptions.EmptyBodyException
import com.clavprogrammer.app_domain.features.player.PlayerSource
import com.clavprogrammer.app_domain.models.ApiErrorResult
import com.clavprogrammer.app_domain.models.Players
import com.clavprogrammer.app_domain.safeCall
import retrofit2.awaitResponse

/*
* Created by Václav Rychtecký on 01/13/2024
*/
class PlayerSourceImpl(
    private val ballDontLieApi: BallDontLieApi,
) : PlayerSource {
    override suspend fun fetchPlayers(params: Int): Result<Players> {
        return safeCall(
            call = { getPlayers(params) },
            errorMessage = "Cannot fetch pokemon"
        )
    }

    private suspend fun getPlayers(params: Int): Result<Players> {
        val response = ballDontLieApi.fetchPlayers(perPage = 35, page = params).awaitResponse()
        return if (response.isSuccessful) {
            val result = response.body()
            if (result?.data != null) {
                Result.Success(data = PlayerMapper.mapToPlayersFromDto(result))
            } else {
                Result.Error(
                    ApiErrorResult(
                        code = response.code(),
                        errorMessage = response.message(),
                        apiThrowable = EmptyBodyException()
                    )
                )
            }
        } else {
            Result.Error(
                ApiErrorResult(
                    code = response.code(),
                    errorMessage = response.message()
                )
            )
        }
    }
}