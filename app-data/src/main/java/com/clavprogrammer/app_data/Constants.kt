package com.clavprogrammer.app_data

/*
* Created by Václav Rychtecký on 01/14/2024
*/
object Constants {
    const val PLAYER_TABLE = "players"
    const val TEAM_TABLE = "teams"
    const val MONETA_DATABASE = "moneta-basketball-database"
}