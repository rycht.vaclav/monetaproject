package com.rychtecky.app_domain.usecases

import com.clavprogrammer.app_domain.Result

/*
* Created by Václav Rychtecký on 12/01/2024
*/
abstract class UseCaseResultNoParams<out T : Any> : UseCase<Result<T>, Unit>() {
    suspend operator fun invoke() = super.invoke(Unit)
}