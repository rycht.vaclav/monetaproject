package com.rychtecky.app_domain.usecases

import com.clavprogrammer.app_domain.Result

/*
* Created by Václav Rychtecký on 12/01/2024
*/
abstract class UseCaseResult<out T : Any, in Params> : UseCase<Result<T>, Params>()