package com.rychtecky.app_domain.usecases

/*
* Created by Václav Rychtecký on 12/01/2024
*/
abstract class UseCase<out T, in Params> {

    /**
     * Executes appropriate implementation of [UseCase],
     * @param params Set of input parameters
     * @return type [T] of parameter. In the most common way the [T] is wrapped to a special use-case implementation.
     */
    suspend operator fun invoke(params: Params): T = doWork(params)

    /**
     * Inner business logic of [UseCase]
     *
     * @param params Set of input parameters
     * @return type [T] of parameter. In the most common way the [T] is wrapped to a special use-case implementation.
     */
    protected abstract suspend fun doWork(params: Params): T
}