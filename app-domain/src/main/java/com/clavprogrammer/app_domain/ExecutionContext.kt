package com.clavprogrammer.app_domain

/*
* Created by Václav Rychtecký on 11/10/2023
*/
sealed class Result<out T : Any> {

    data class Success<out T : Any>(val data: T) : Result<T>()
    data class Error<out T : Any>(val error: ErrorResult, val data: T? = null) : Result<T>()

    fun isSuccess() = this is Success

    fun isError() = this is Error

    fun errorOrNull() = when (this) {
        is Error -> error
        else -> null
    }

    fun getOrNull() = when (this) {
        is Success -> data
        is Error -> data
        else -> null
    }
}

open class ErrorResult(open var message: String? = null, open var throwable: Throwable? = null)

suspend fun <T : Any> safeCall(call: suspend () -> Result<T>, errorMessage: String): Result<T> {
    return try {
        call()
    } catch (e: Throwable) {
        Result.Error(ErrorResult(errorMessage, e))
    }
}