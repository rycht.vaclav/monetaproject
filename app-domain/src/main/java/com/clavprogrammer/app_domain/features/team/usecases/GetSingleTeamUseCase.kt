package com.clavprogrammer.app_domain.features.team.usecases

import com.clavprogrammer.app_domain.Result
import com.clavprogrammer.app_domain.features.team.TeamRepository
import com.clavprogrammer.app_domain.models.Team
import com.rychtecky.app_domain.usecases.UseCaseResult

/*
* Created by Václav Rychtecký on 01/14/2024
*/
class GetSingleTeamUseCase(private val teamRepository: TeamRepository) :
    UseCaseResult<Team, Int>() {
    override suspend fun doWork(params: Int): Result<Team> =
        teamRepository.getTeamById(params)
}