package com.clavprogrammer.app_domain.features.player

import com.clavprogrammer.app_domain.Result
import com.clavprogrammer.app_domain.models.Player
import com.clavprogrammer.app_domain.models.Players

/*
* Created by Václav Rychtecký on 01/13/2024
*/
interface PlayerRepository {
    suspend fun getPlayers(params: Int): Result<Players>
    suspend fun getSinglePlayer(params: Int): Result<Player>
}