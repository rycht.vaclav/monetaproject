package com.clavprogrammer.app_domain.features.player.usecases

import com.clavprogrammer.app_domain.Result
import com.clavprogrammer.app_domain.features.player.PlayerRepository
import com.clavprogrammer.app_domain.models.Players
import com.rychtecky.app_domain.usecases.UseCaseResult

/*
* Created by Václav Rychtecký on 01/13/2024
*/
class FetchPlayersUseCase(private val playerRepository: PlayerRepository) :
    UseCaseResult<Players, Int>() {
    override suspend fun doWork(params: Int): Result<Players> = playerRepository.getPlayers(params)
}