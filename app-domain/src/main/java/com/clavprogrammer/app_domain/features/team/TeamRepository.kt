package com.clavprogrammer.app_domain.features.team

import com.clavprogrammer.app_domain.Result
import com.clavprogrammer.app_domain.models.Team

/*
* Created by Václav Rychtecký on 01/14/2024
*/
interface TeamRepository {
    suspend fun getTeamById(params: Int): Result<Team>
}