package com.clavprogrammer.app_domain.features.player.usecases

import com.clavprogrammer.app_domain.Result
import com.clavprogrammer.app_domain.features.player.PlayerRepository
import com.clavprogrammer.app_domain.features.player.PlayerSource
import com.clavprogrammer.app_domain.models.Player
import com.rychtecky.app_domain.usecases.UseCaseResult

/*
* Created by Václav Rychtecký on 01/14/2024
*/
class GetSinglePlayerUseCase(private val playerRepository: PlayerRepository) :
    UseCaseResult<Player, Int>() {
    override suspend fun doWork(params: Int): Result<Player> =
        playerRepository.getSinglePlayer(params)
}