package com.clavprogrammer.app_domain.exceptions

import java.io.IOException

/*
* Created by Václav Rychtecký on 01/13/2024
*/
class EmptyBodyException : IOException("Response with empty body")