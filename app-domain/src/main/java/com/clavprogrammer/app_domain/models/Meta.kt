package com.clavprogrammer.app_domain.models

/*
* Created by Václav Rychtecký on 01/13/2024
*/
data class Meta(
    val totalPages: Int,
    val currentPage: Int,
    val nextPage: Int
)