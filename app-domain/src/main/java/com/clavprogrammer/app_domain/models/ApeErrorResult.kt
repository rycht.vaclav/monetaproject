package com.clavprogrammer.app_domain.models

import com.clavprogrammer.app_domain.ErrorResult

/*
* Created by Václav Rychtecký on 01/13/2024
*/
data class ApiErrorResult(
    val code: Int,
    val errorMessage: String? = null,
    val apiThrowable: Throwable? = null
) : ErrorResult(errorMessage, apiThrowable)