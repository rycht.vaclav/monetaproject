package com.clavprogrammer.app_domain.models

/*
* Created by Václav Rychtecký on 01/12/2024
*/
data class Player(
    val id: Int,
    val firstName: String,
    val lastName: String,
    val position: String,
    val heightFeet: Int?,
    val heightInches: Int?,
    val weightPounds: Double?,
    val team: Team
)