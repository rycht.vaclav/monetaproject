package com.clavprogrammer.app_domain.models

/*
* Created by Václav Rychtecký on 01/13/2024
*/
data class Players(
    val data: List<Player>,
    val meta: Meta
)