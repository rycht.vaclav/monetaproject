package com.clavprogrammer.app_domain.models

/*
* Created by Václav Rychtecký on 01/12/2024
*/
data class Team(
    val id: Int,
    val abbreviation: String,
    val city: String,
    val conference: String,
    val division: String,
    val fullName: String,
    val name: String
)